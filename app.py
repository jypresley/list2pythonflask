#written by Presley Lacharmante (c) 2019
from flask import Flask, render_template, flash, redirect, url_for, session, logging, request, abort
from data import Stuffs
from flask_mysqldb import MySQL
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from gevent.pywsgi import WSGIServer


app = Flask(__name__)

# MySQL configurations
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_DB'] = 'list2flask'
app.config['MYSQL_USER'] = 'python'
app.config['MYSQL_PASSWORD'] = 'python'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

#init MYSQL
mysql = MySQL(app)

Stuffs = Stuffs()

@app.route('/')
def index():
    return render_template('home.html')

#get call

@app.route('/stuffs')
def stuffs():
    return render_template('stuffs.html', stuffs = Stuffs)

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/stuff/<string:id>/')
def stuff(id):
    for i in Stuffs:
        if i['id'] == id:
            break
    return render_template('stuff.html', id = id, i = i)

#using mysql (to test)
@app.route('/stuffs_mysql')
def stuffs_mysql():
    return render_template('stuffs_mysql.html', stuffs = Stuffs)

@app.route('/stuff_mysql/<string:id>/')
def stuff_mysql(id):
    return render_template('stuff_mysql.html', id = id)

#using postgreSQL (to test)
@app.route('/stuffs_postgresql')
def stuffs_postgresql():
    return render_template('stuffs_postgresql.html', stuffs = Stuffs)

@app.route('/stuff_postgresql/<string:id>/')
def stuff_postgresql(id):
    return render_template('stuff_postgresql.html', id = id)

#form 

class RegisterForm(Form):
    name = StringField('Name', [validators.Length(min=1, max=50)])
    username = StringField('Username', [validators.Length(min=1, max=50)])
    email = StringField('Email', [validators.Length(min=6, max=50)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Password do not match')
    ])
    confirm = PasswordField('Confirm Password')

@app.route('/register', methods=['GET','POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        name = form.name.data
        email = form.email.data
        username = form.username.data
        password = sha256_crypt.encrypt(str(form.password.data))

        #Create cursor
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO users(name, email, username, password) VALUES(%s, %s, %s, %s)", (name, email, username, password))

        # Commit to DB
        mysql.connection.commit()

        #close connection
        cur.close()

        flash('you are now registered and can log in', 'success')

        return redirect(url_for('login'))

    return render_template('register.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.form == 'POST':
        #get form fields
        username = request.form['username']
        password_candidate = request.form['password']

        #create cursor
        cur = mysql.connection.cursor()

        #get user by username
        result = cur.execute('SELECT * from users where username= %s', [username])

        if result > 0:
            #get stored hash
            data = cur.fetchone()
            password = data['password']

            #compare password
            if sha256_crypt.verify(password_candidate, password):
                app.logger.info("PASSWORD MATCHED")
            else:
                app.loggger.info("PASSWORD NOT MATCHED")
        else:
            app.logger.info("NO USER")

    #abort(401)
    return render_template('login.html')

#execute
if __name__ == '__main__':
    app.secret_key = "presley123"

    # Development
    app.run(debug=True)

    # Production
    #http_server = WSGIServer(('', 5000), app)
    #http_server.serve_forever()


