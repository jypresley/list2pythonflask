def Stuffs():
    stuffs = [
                {
                    'id' : '1',
                    'item' : 'coca cola',
                    'details' : 'Coca-Cola, parfois abrégé Coca ou Cola dans les pays francophones ou Coke en Amérique du Nord et dans certains pays européens et africains, est une marque nord américaine de soda de type cola fabriquée par The Coca-Cola Company. Cette marque a été déposée en 1886.',
                    'owner' : 'Presley',
                    'create_date' : '04-24-2019'
                },
                {
                    'id' : '2',
                    'item' : 'laptop',
                    'details' : 'Shop a wide selection of Laptops including 2 in 1 and traditional laptops at Amazon.com. Free shipping and free returns on eligible items.',
                    'owner' : 'Presley',
                    'create_date' : '04-24-2019'
                },
                {
                    'id' : '3',
                    'item' : 'cravatte',
                    'details' : 'Un choix unique de Cravatte homme disponible dans notre magasin. Codes promo, ventes flash, livraison offerte, trouvez le produit de vos rêves à prix réduit !',
                    'owner' : 'Presley',
                    'create_date' : '04-24-2019'
                }
            ]
    return stuffs

    

